import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { PartidoService } from "../partidos.service";

@Component({
  selector: "app-add",
  templateUrl: "./add.page.html",
  styleUrls: ["./add.page.scss"],
})
export class AddPage implements OnInit {
  formAdd: FormGroup;

  constructor(private servicePartido: PartidoService, private router: Router) {}

  ngOnInit() {
    //validations inputs

    this.formAdd = new FormGroup({
      pequipo1: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pequipo2: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      pfecha: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      pestadio: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
    });
  }

  addPartido() {
    //add product to array

    if (!this.formAdd.valid) {
      return;
    }

    this.servicePartido.addPartido(
      this.formAdd.value.pidpartido,
      this.formAdd.value.pequipo1,
      this.formAdd.value.pmarcador1,
      this.formAdd.value.pequipo2,
      this.formAdd.value.pmarcador2,
      this.formAdd.value.pfecha,
      this.formAdd.value.pestadio
    );

    this.formAdd.reset(); //clean form

    this.router.navigate(["/partidos"]);
  }
}
