import { Injectable } from "@angular/core";
import { partidos, eventos } from "./partidos.model";

@Injectable({
  providedIn: "root",
})
export class PartidoService {
  //array eventos
  private evento: eventos[] = [
    {
      idPartido: 1,
      evento: "Falta",
      equipo: "Alajuela",
      jugador: "Pablo Soto",
      minuto: 50,
    },
  ];

  //array partidos
  private partido: partidos[] = [
    {
      eventos: this.evento,
      idPartido: 1,
      equipo1: "LDA",
      marcador1: 0,
      equipo2: "Heredia",
      marcador2: 0,
      fecha: "2020-12-30",
      estadio: "Estadio Morera Soto",
      boton: 0,
    },
  ];
  constructor() {}

  // get all elements to array
  getAll() {
    return [...this.partido];
  }

  //get elements for ID

  getPartido(partidoId: number) {
    return {
      ...this.partido.find((partido) => {
        return partido.idPartido === partidoId;
      }),
    };
  }

  setBoton(code: number) {
    let index = this.partido.map((x) => x.idPartido).indexOf(code);
    this.partido[index].boton = 1;
  }

  //add products to array

  addPartido(
    idpartido: number,
    pequipo1: string,
    pmarcador1: number,
    pequipo2: string,
    pmarcador2: number,
    pfecha: string,
    pestadio: string
  ) {
    const partido: partidos = {
      eventos: this.evento,
      idPartido: Math.round(Math.random() * (100 - 3) + 3),
      equipo1: pequipo1,
      marcador1: 0,
      equipo2: pequipo2,
      marcador2: 0,
      fecha: pfecha.substr(0, 10),
      estadio: pestadio,
      boton: 0,
    };

    this.partido.push(partido);
  }

  addEvento(
    idpartido: number,
    pevento: string,
    pequipo: string,
    pjugador: string,
    pminuto: number
  ) {
    const evento: eventos = {
      idPartido: idpartido,
      evento: pevento,
      equipo: pequipo,
      jugador: pjugador,
      minuto: pminuto,
    };

    if (evento.evento == "Gol") {
      let index = this.partido.map((x) => x.idPartido).indexOf(idpartido); //get the index of the element

      if (this.partido[index].equipo1 == evento.equipo) {
        this.partido[index].marcador1++;
      } else {
        this.partido[index].marcador2++;
      }
    }

    this.evento.push(evento);
  }

  //edit products, according to the index of the selected element
  /*
  editProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string
  ) {
    let index = this.productos.map((x) => x.codigo).indexOf(pcodigo); //get the index of the element

    this.productos[index].codigo = pcodigo;
    this.productos[index].candidad = pcantidad;
    this.productos[index].nombre = pnombre;
    this.productos[index].peso = ppeso;
    this.productos[index].fecha_caducidad = pfecha_caducidad;
    this.productos[index].descripcion = pdescripcion;
    this.productos[index].precio = pprecio;

    //second option

     let myObj = this.productos.find(ob => ob.codigo = pcodigo);

    myObj.candidad= pcantidad;
    myObj.precio= pprecio;
    myObj.nombre= pnombre;
    myObj.peso= ppeso;
    myObj.descripcion= pdescripcion;
    myObj.fecha_caducidad= pfecha_caducidad;

    this.productos[index] = myObj;

    //console.log(this.productos);
  }*/
}
