import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { PartidoService } from "../partidos.service";
import { partidos } from "../partidos.model";

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {

  formAdd: FormGroup;
  idPart: number;
  part: partidos;

  constructor(  
    private activeRouter: ActivatedRoute,
    private partidosService: PartidoService,
    private router: Router) { }

  ngOnInit() {  
    
    //validations inputs

    this.formAdd = new FormGroup({
      pevento: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pequipo: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      pjugador: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      pminuto: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),     
    });   
    
    //obtain the parameter to search the product and then the index

      this.activeRouter.paramMap.subscribe((paramMap) => {
        if (!paramMap.has("partidoId")) {
          return;
        }
        const partId = parseInt(paramMap.get("partidoId"));
        this.part = this.partidosService.getPartido(partId);
        this.idPart = partId;
      });

  }

  addEvento() {
    //add evento to array

    if (!this.formAdd.valid) {
      return;
    }

    this.partidosService.addEvento(
      this.formAdd.value.idPartidos = this.idPart,
      this.formAdd.value.pevento,
      this.formAdd.value.pequipo,
      this.formAdd.value.pjugador,
      this.formAdd.value.pminuto
    );

    this.formAdd.reset(); //clean form

    this.router.navigate(["/partidos"]);
  }
}
