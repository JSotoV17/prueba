
export interface partidos{
    
    idPartido:number;
    equipo1: string;
    marcador1: number
    equipo2: string;
    marcador2:number;
    fecha: string;
    estadio: string;
    boton:number;
    eventos: eventos[];
}

export interface eventos{
    idPartido:number;
    evento: string;  
    equipo: string;
    jugador:string;
    minuto: number;
}