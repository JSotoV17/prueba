import { Component, OnInit } from "@angular/core";
import { PartidoService } from "./partidos.service";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { NavController, ToastController } from "@ionic/angular";
import { partidos } from "./partidos.model";

@Component({
  selector: 'app-partidos',
  templateUrl: './partidos.page.html',
  styleUrls: ['./partidos.page.scss'],
})
export class PartidosPage implements OnInit {

    partido: partidos[];

  constructor(
    private partidoServices: PartidoService,
    private router: Router,
    public navCtrl: NavController,
    public toastCtrl: ToastController
  ) {}

  ngOnInit() {

    //load initial data
      this.partido = this.partidoServices.getAll();
      console.log (this.partido);
    }

      // data load before being loaded
  ionViewWillEnter() {
    this.partido = this.partidoServices.getAll();
  }

    //details partido
  view(code: number) {
    this.router.navigate(["/partidos/detail/" + code]);
  }
  
   //message to logout
   async logout() {
    this.navCtrl.navigateRoot("/");

    const toast = await this.toastCtrl.create({
      message: "Cierre Correcto.",
      duration: 2000,
      cssClass: "toast-message",
    });
    toast.present();
  }

}
