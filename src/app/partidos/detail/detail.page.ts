import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { PartidoService } from "../partidos.service";
import { partidos } from "../partidos.model";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.page.html",
  styleUrls: ["./detail.page.scss"],
})
export class DetailPage implements OnInit {
  part: partidos;
  public disabled = false;
  id: number;

  constructor(
    private activeRouter: ActivatedRoute,
    private partidosService: PartidoService,
    private router: Router,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    //obtain the parameter to search the product and then the index
    this.activeRouter.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("partidoId")) {
        return;
      }
      const partidoId = parseInt(paramMap.get("partidoId"));
      this.part = this.partidosService.getPartido(partidoId);
      this.id = partidoId;

      if (this.part.boton == 0) {
        this.disabled = false;
      } else {
        this.disabled = true;
      }
    });
  }

  //details eventos
  view(code: number) {
    this.router.navigate(["/partidos/events/" + code]);
  }

  public action() {
    this.partidosService.setBoton(this.id);
    this.disabled = true;
  }
}
