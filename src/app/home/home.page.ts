import { Component } from "@angular/core";
import {
  NavController,
  LoadingController,
  ToastController,
} from "@ionic/angular";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) {}

  //button to redirect products page

  principal() {
    this.navCtrl.navigateRoot("/partidos");
    this.message_start();
  }

 // message to wait while data is loaded

  async message_start() {
    const loading = await this.loadingCtrl.create({
      message: "Cargando Información...",
      duration: 2000, //seconds
    });
    loading.present();

    const toast = await this.toastCtrl.create({
      message: "Bienvenido(a)",
      duration: 2000, //seconds
      cssClass: "toast-message",
    });
    toast.present();
  }
}
